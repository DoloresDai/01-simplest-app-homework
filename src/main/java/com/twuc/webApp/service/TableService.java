package com.twuc.webApp.service;

import org.springframework.stereotype.Service;

@Service
public class TableService {
    public static String tableOperations(String operation) {
        StringBuilder tableResult = new StringBuilder();
        int i, j;
        for (i = 1; i < 10; i++) {
            for (j = 1; j <= i; j++) {
                int result = operation.equals("+") ? i + j : i * j;
                String format = String.format("%d%s%d%s%d", i, operation, j, "=", result);
                tableResult.append(format);
                if (i != j) {
                    tableResult.append(result >= 10 ? " " : "  ");
                }
            }
            tableResult = tableResult.append("\n");
        }
        return String.valueOf(tableResult);
    }
}
