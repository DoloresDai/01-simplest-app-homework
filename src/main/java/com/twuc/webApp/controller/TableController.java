package com.twuc.webApp.controller;

import com.twuc.webApp.enums.Enums;
import com.twuc.webApp.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.twuc.webApp.service.TableService.tableOperations;

@RestController
@RequestMapping("/api/tables")
public class TableController {

    @Autowired
    private TableService tableService;


    @GetMapping("/plus")
    String tablePlus() {
        return tableOperations(Enums.ADD.getOperation());
    }

    @GetMapping("/multiply")
    String tableMultiply() {
        return tableOperations(Enums.MULTIPLY.getOperation());
    }
}
