package com.twuc.webApp.enums;

public enum Enums {
    ADD("+"),
    MULTIPLY("*");

    private String operation;

    Enums(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }
}
